/* 
   Xsellogd: X Selection Logger Daemon

   Copyright 2016-2017 Lasse Pouru

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <getopt.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <syslog.h>
#include <time.h>
#include <unistd.h>
#include <X11/Xlib.h>
#include <X11/Xatom.h>

#define PROGRAM_NAME "xsellogd"
#define DEFAULT_PATH strcat(getenv("HOME"), "/xsellogd.log")

static volatile sig_atomic_t quit = 0;
static pid_t pid;

/* Variables for capturing X events */
static Window win;  
static Display *dpy;
static XEvent event;
static Atom type;
static int format;
static unsigned long numItems;
static unsigned long bytesLeft;
static unsigned char *data; // selected text will be saved here

/* Variables for output and time options */
static char* outputPath;
static FILE *fp;
static int includeTime;
static time_t now;
struct tm* calendar_time;
static char timeBuffer[40];
static struct option longOpts[] = {
  {"help",    no_argument,       NULL, 'h'},
  {"outfile", required_argument, NULL, 'o'},
  {"time",    no_argument,       NULL, 't'},
  {NULL,      0,                 NULL, 0  }};
    
static void help (char* programName) {
  /* Print help and exit */
  printf("Usage: %s [OPTION]...\n", programName);
  printf("Log PRIMARY selection to file.\n\n");
  printf("  -o, --outfile\tspecify output file; defaults to %s\n", DEFAULT_PATH);
  printf("  -h, --help\tshow this help and exit\n");
  exit(EXIT_SUCCESS);	
}

static void parseOptions (int argc, char* argv[]) {
  /* Set option flags */
  int c = 0, h = 0, o = 0, t = 0;               
  while((c = getopt_long(argc, argv, "o:th", longOpts, NULL)) != -1) {               
    switch (c) {
      case 0:
	break;
      case 'h': // help 
	h = 1;
	break;
      case 'o': // user-defined outfile
        outputPath = optarg;
	o = 1;
        break;
      case 't': // include time in output
        t = 1;
        break;
      default:
	printf("Try '%s --help' for more information.\n", argv[0]);
	exit(EXIT_FAILURE);
    }
  }
  if (h) {
    help(argv[0]);
    exit(EXIT_SUCCESS);
  }
  if (!o)
    outputPath = DEFAULT_PATH;
  includeTime = t;
  return;
}

static void sigHandler(int signal) {
  switch(signal) {
    case SIGINT:
    case SIGTERM:
    case SIGSEGV:
      quit = 1;
    default:
      break;
  }
}

static int errHandler (Display *display, XErrorEvent *error) {
  /* Log any xlib-related errors and continue */
  syslog(LOG_ERR, "%s: xlib error: %m", PROGRAM_NAME);
  return 0;
}

static pid_t daemonize() {
  pid_t pid;
       
  /* Fork and exit parent */
  pid = fork();
  if (pid < 0) {
    syslog(LOG_ERR, "%s: could not fork: %m", PROGRAM_NAME);
    closelog();
    exit(EXIT_FAILURE);
  }
  else if (pid > 0)
    exit(EXIT_SUCCESS);
  
  /* Create a new SID for the child process */
  if (setsid() < 0) {
    syslog(LOG_ERR, "%s: could not create a session ID: %m", PROGRAM_NAME);
    closelog();
    exit(EXIT_FAILURE);
  }
        
  /* Set signal and error handlers */
  signal(SIGHUP, sigHandler);
  signal(SIGINT, sigHandler);
  signal(SIGTERM, sigHandler);
  signal(SIGSEGV, sigHandler);
  XSetErrorHandler(errHandler);

  /* Fork and exit parent again */
  pid = fork();
  if (pid < 0) {
    syslog(LOG_ERR, "%s: could not fork: %m", PROGRAM_NAME);
    closelog();
    exit(EXIT_FAILURE);
  }
  else if (pid > 0)
    exit(EXIT_SUCCESS);

  /* Change the working directory to root */
  if ((chdir("/")) < 0) {
    syslog(LOG_ERR, "%s: could not change working directory: %m", PROGRAM_NAME);
    closelog();
    exit(EXIT_FAILURE);
  }
        
  /* Change the file mode mask */
  umask(0);
  
  /* Close all open file descriptors */
  for (int fd = sysconf(_SC_OPEN_MAX); fd > 0; fd--)
    close (fd);
  
  return getpid();
}

int main(int argc, char *argv[]) {  
  /* Parse options */
  parseOptions(argc, argv);
 
  /* Open log */
  openlog(PROGRAM_NAME, LOG_ODELAY, LOG_DAEMON);

  /* Become daemon */
  pid = daemonize();
 
  /* Connect to X server, create window for receiving events */  
  if (!(dpy = XOpenDisplay(NULL))) {
    syslog(LOG_ERR, "%s: could not open display: %m", PROGRAM_NAME);
    closelog();
    exit(EXIT_FAILURE);
  }
  win = XCreateSimpleWindow(dpy, DefaultRootWindow(dpy), 0, 0, 1, 1, 0, 0, 0);
  
  /* Open output file */
  fp = fopen(outputPath, "a");
  if (fp == NULL) {
    syslog(LOG_ERR, "%s: could not open file %s: %m", PROGRAM_NAME, outputPath);
    XCloseDisplay(dpy);
    closelog();
    exit(EXIT_FAILURE);
  }
    
  while (!quit) {

    /* Request selection data */
    XConvertSelection(dpy, XA_PRIMARY, XA_STRING, None, win, CurrentTime); // if possible, convert PRIMARY selection to string
    XSetSelectionOwner(dpy, XA_PRIMARY, win, CurrentTime); // become owner of PRIMARY selection
    XGetWindowProperty (dpy, win, XA_STRING, 0, 0, 0, XA_STRING, &type, &format, &numItems, &bytesLeft, &data); // get amount of bytes to read
    if (bytesLeft > 0) {
      if (XGetWindowProperty (dpy, win, XA_STRING, 0, bytesLeft, 0, XA_STRING, &type, &format, &numItems, &bytesLeft, &data) == Success) { // read selection contents to data variable

	/* Wait for new selection to be made */
        XNextEvent(dpy, &event);
	while (event.type != SelectionClear) // when new selection is made, we lose ownership of PRIMARY selection and receive a SelectionClear event
	  XNextEvent(dpy, &event);
	
	/* Write time and currently saved selection to file */
	if (includeTime) {
	  time(&now);
	  calendar_time = localtime(&now);
	  strftime(timeBuffer, 42, "%c", calendar_time);
	  fprintf(fp, "%s\n", timeBuffer);
	}
	fprintf(fp, "%s\n\n", data);
      }
    }
  }
  fclose(fp);
  XFree(data);
  XCloseDisplay(dpy);
  closelog();
  exit(EXIT_SUCCESS);
}
