PROG	   = xsellogd
CC	   = gcc
PREFIX	  ?= /usr/local
BINPREFIXi =  ${PREFIX}/bin

CFLAGS	= -Wall
LIBS	= -lX11
CFLAGS	+= `pkg-config --cflags x11`
LIBS	+= `pkg-config --libs x11`

${PROG}: ${PROG}.c
	@${CC} ${CFLAGS} ${LIBS} -o ${PROG} ${PROG}.c
	@strip ${PROG}

install:
	install -Dm755 ${PROG} ${DESTDIR}${BINPREFIX}/${PROG}

uninstall:
	rm -f ${BINPREFIX}/${PROG}

clean:
	rm -f ${PROG}
