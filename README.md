# XSELLOGD #

### Summary ###

Xsellogd is a daemon that logs the contents of the PRIMARY X Window selection (i.e. clipboard) to a file.

### Usage ###

Xsellogd runs as a daemon and currently does not require root privileges. Probably the simplest way to autostart xsellogd would be to add it to .xinitrc (but before you do, see TODO section below).

Output file path may be set as an argument to the -o (--outfile) option. The default is $HOME/xsellogd.log.

Times and dates of selections may be included in the output with the -t (--time) option.

### Installation ###

make && make install

### Shared object dependencies ###

- linux-vdso.so.1
- libX11.so.6
- libc.so.6
- libxcb.so.1
- libdl.so.2
- libXau.so.6
- libXdmcp.so.6
- ld-linux.so.2
	
### TODO ###

- fix CPU resource hogging when started from .xinitrc
- fix bug that logs the current selection twice at startup
- write init script
- (write systemd unit file)
- implement pid file for stopping and restarting
